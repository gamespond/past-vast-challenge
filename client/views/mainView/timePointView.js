d3.timePointView = function() {
    var width, height;

    function timePointView(g, data) {

        var padding = 7;
        var contiPath = discrete2conti(data);
        var xscale = d3.scale.linear()
            .domain([0, 86399])
            .range([padding, width - padding]);

        var xscaleR = d3.scale.linear()
            .domain([padding, width - padding])
            .range([0, 86399]);

        var xAxis = d3.svg.axis()
            .scale(xscale)
            .orient("bottom")
            // .ticks(4)
            .tickValues(chooseValues(contiPath))
            .tickFormat(function(d) {
                return formatHour(d);
            })

        g
            .selectAll('rect')
            .data(contiPath)
            .enter()
            .append('rect')
            .attr('class', 'timePointView')
            .attr('x', function(d) {
                return xscale(d.start);
            })
            .attr('y', function(d) {
                return 0
            })
            .attr('width', function(d) {
                var result = xscale(d.end) - xscale(d.start);
                if (result < 0) {
                    console.log(d)
                }
                return result
            })
            .attr('height', function(d) {
                return height;
            })
            .attr('fill', function(d) {
                if (d.type === 'check-in') {
                    return '#fbb4ae';
                } else {
                    return '#b3cde3'
                }
            })

        var axis = g.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(" + 0 + "," + 10 + ")")
            .call(xAxis);

        var overlay = g
            .append('rect')
            .attr('class', 'overlay')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', width)
            .attr('height', 30)
            .attr('fill', 'white')
            .attr('opacity', '0')
            .on('mousemove', function(d) {
                var coord = d3.mouse(this)
                var mouseTS = data[0]['Timestamp'].split(' ')[0] + ' ' + formatHour(Math.round(xscaleR(coord[0])));
                Session.set('mouseTS', mouseTS)
            })
            .on('click', function(d){
                var coord = d3.mouse(this)
                var mouseTS = data[0]['Timestamp'].split(' ')[0] + ' ' + formatHour(Math.round(xscaleR(coord[0])));
                Session.set('selectedTime', mouseTS);
            })

    }

    var chooseValues = function(contiPath) {
        var start = contiPath[0].start;
        var end = contiPath[contiPath.length - 1].end;

        var first = Math.round(start / 2),
            middle = Math.round(start + (end - start) / 2),
            last = Math.round(end + (86400 - end) / 2);
        return [first, start, middle, end];
    }
    var discrete2conti = function(data) {
        data.sort(function(a, b) {
            return moment(a['Timestamp']).unix() - moment(b['Timestamp']).unix();
        })
        var arr = []
        var currentStep = data[0];
        arr.push({
            type: currentStep['type'],
            start: str2sec(currentStep['Timestamp']),
            end: str2sec(currentStep['Timestamp'])
        });
        for (var i = 1; i < data.length; i++) {
            var step = data[i];
            if (step.type === currentStep.type) {
                arr[arr.length - 1].end = str2sec(step['Timestamp']);
            } else {
                arr[arr.length - 1].end = str2sec(step['Timestamp']);
                arr.push({
                    type: step['type'],
                    start: str2sec(step['Timestamp']),
                    end: str2sec(step['Timestamp'])
                });
                currentStep = step;
            }
        }
        return arr;
    }

    var str2sec = function(str) {
        var inputFormat = 'YYYY-M-DD HH:mm:ss'
        var parse = moment(str, inputFormat);
        return parse.hour() * 3600 + parse.minute() * 60 + parse.second();
    }

    var formatHour = function(num) {
        var hour = Math.floor(num / 3600);
        var minute = Math.floor((num - hour * 3600) / 60);
        var second = num - hour * 3600 - minute * 60;
        if (hour < 10) hour = '0' + hour;
        if (minute < 10) minute = '0' + minute;
        if (second < 10) second = '0' + second;
        return '' + hour + ':' + minute + ':' + second;
    }

    timePointView.width = function(x) {
        var res;
        if (!arguments.length) {
            res = width;
        } else {
            width = x;
            res = timePointView;
        }
        return res;
    };

    timePointView.height = function(x) {
        var res;
        if (!arguments.length) {
            res = height;
        } else {
            height = x;
            res = timePointView;
        }
        return res;
    };


    return timePointView;
}
